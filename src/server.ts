#!/usr/bin/env node

/**
 * Dependencies
 */

import * as http from "http";
import * as mongoose from "mongoose";
import { app } from "./index";

/**
 * Get port from environment and store in Express
 */

const port = normalizePort(process.env.PORT || "3005");
app.set("port", port);

// connect mongoose danata database eka connect nokara imu
// mongoose.connect("mongodb://testuser:test@ds161346.mlab.com:61346/tracified-backend-test-db",
// { useMongoClient: true });
// (mongoose as any).Promise = global.Promise;
/**
 * Create HTTP server
 */

const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val: any) {

  if (isNaN(parseInt(val, 10))) {
    return val;
  }

  if (parseInt(val, 10) >= 0) {
    return parseInt(val, 10);
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error: any) {
  if (error.syscall !== "listen") {
    throw error;
  }

  const bind = typeof port === "string"
    ? "Pipe " + port
    : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + ' requires elevated privileges'); // tslint:disable-line
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + ' is already in use'); // tslint:disable-line
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = server.address();
  const bind = typeof addr === "string"
    ? "pipe " + addr
    : "port " + addr.port;
  console.log("Listening on " + bind); // tslint:disable-line
}

import { NextFunction, Request, Response, Router } from "express";


const router = Router();

router.get("/", (req: Request, res: Response, next: NextFunction) => {
  res.json({
    apiVersion: "1.0",
    msg: "hello world!!!"
  });
});

export { router };

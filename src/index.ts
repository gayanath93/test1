// import * as bodyParser from "body-parser";
import * as express from "express";
import * as mongoose from "mongoose";
import * as apiV1 from "./routes/v1";

/**
 * Create Express Server
 */
const app = express();

/**
 * Express Configuration
 */

app.set("port", process.env.PORT || 3000);

/**
 * API v1 Routes
 */

app.use("/api/v1", apiV1.router);

/**
 * MongoDB Connection
 */

 // Connect to Mongoose
// mongoose.connect("mongodb://tracified-backend-user:weare99x@ds161455.mlab.com:61455/tracified-backend");
// const db = mongoose.connection;

export { app };
